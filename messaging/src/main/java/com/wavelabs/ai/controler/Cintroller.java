package com.wavelabs.ai.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Student;

@RestController
public class Cintroller {

	@Autowired
	private KafkaTemplate<String, Student> kafkaTemplate;

	@PostMapping("send")
	public ResponseEntity<Student> sendMessage(@RequestBody Student student) {

		kafkaTemplate.send("test", student);
		return ResponseEntity.status(200).body(student);
	}

}
