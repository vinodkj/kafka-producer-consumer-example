package com.wavelabs.ai.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.wavelabs.ai.model.Student;

@Configuration
public class KafkaProducer {

	@Bean
	public static ProducerFactory<String, Student> getProducerProperties() {

		Map<String, Object> props = new HashMap<>();

		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonSerializer.class);
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, "300");
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "900000");
		props.put(ProducerConfig.ACKS_CONFIG, "1");
		props.put(ProducerConfig.RETRIES_CONFIG, "3");

		return new DefaultKafkaProducerFactory<String, Student>(props);

	}

	@Bean
	public static KafkaTemplate<String, Student> kafkaTemplate() {
		return new KafkaTemplate<String, Student>(getProducerProperties());
	}

}
