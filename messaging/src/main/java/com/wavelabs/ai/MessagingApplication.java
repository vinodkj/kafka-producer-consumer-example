package com.wavelabs.ai;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class MessagingApplication {

	/*
	 * @Autowired private KafkaTemplate<String, String> kafkaTemplate;
	 * 
	 * public void sendMessage(String msg) { kafkaTemplate.send("test", msg); }
	 */ public static void main(String[] args) {
		SpringApplication.run(MessagingApplication.class, args);
	}

	/*
	 * @KafkaListener(topics = "test", groupId = "group_id") public void
	 * listen(String message) {
	 * System.out.println("Received Messasge in group - group-id: " + message); }
	 * 
	 * @Override public void run(ApplicationArguments args) throws Exception {
	 * sendMessage("Hi Welcome to Spring For Apache Kafka"); }
	 */

	@KafkaListener(topics = "test", groupId = "group_id")
	public void listen(String message) {

		JSONObject json = new JSONObject(message);

		String name = json.getString("name");
		String email = json.getString("email");
		Integer id = json.getInt("id");
		String bloodGroup = json.getString("bloodGroup");

		System.out.println(" name is=" + name);
		System.out.println("email" + email);
		System.out.println("id" + id);
		System.out.println("bloodGroup" + bloodGroup);

	}

}
