package com.wavelabs.ai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaSampleMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaSampleMessagingApplication.class, args);
	}

}
