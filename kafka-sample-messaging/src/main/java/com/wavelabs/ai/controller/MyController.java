package com.wavelabs.ai.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Employee;
import com.wavelabs.ai.model.Student;
import com.wavelabs.ai.service.StudentService;

@RestController
public class MyController {

	@Autowired
	private StudentService studentService;

	@PostMapping("send/student")
	public Student sendStudent(@RequestBody Student student) {
		studentService.sendMessage(student);

		return student;
	}

	@PostMapping("send/employee")
	public Employee sendEmployee(@RequestBody Employee employee) {
		studentService.sendEmployee(employee);
		return employee;

	}

	@GetMapping("consume/student")
	@KafkaListener(topics = "student", groupId = "group_id")
	public String consumeStudent(String message) {
		return message;

	}

	@GetMapping("consume/employee")
	@KafkaListener(topics = "employee", groupId = "group_id")
	public String consumeEmployee(String message) {
		return message;

	}

}
