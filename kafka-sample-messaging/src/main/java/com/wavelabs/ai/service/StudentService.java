package com.wavelabs.ai.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Employee;
import com.wavelabs.ai.model.Student;

@Service
public class StudentService {

	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;

	public void sendMessage(Student student) {
		kafkaTemplate.send("student", student);
		System.out.println(" message send successfullly");

	}

	@KafkaListener(topics = "student", groupId = "group1")
	public String listenStudent(String message) {

		return message;
	}

	public void sendEmployee(Employee emp) {
		kafkaTemplate.send("employee", emp);
		System.out.println(" employee information send successfully");
	}

	@KafkaListener(topics = "employee", groupId = "group1")
	public String listenEmployee(String message) {

		return message;
	}

}
