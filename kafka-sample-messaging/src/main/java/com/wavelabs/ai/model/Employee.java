package com.wavelabs.ai.model;

public class Employee {

	private Integer id;
	private String email;
	private String bloodGroup;
	private String companyName;
	
	public Employee()
	{
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Employee(Integer id, String email, String bloodGroup, String companyName) {
		super();
		this.id = id;
		this.email = email;
		this.bloodGroup = bloodGroup;
		this.companyName = companyName;
	}

}
